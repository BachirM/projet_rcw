import React from 'react';
import Connexion from './Components/Connexion/connexion';
import Inscription from './Components/Inscription/inscription';
import Prediction from './Components/Prediction/prediction';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/connexion" component={Connexion} />
          <Route path="/inscription" component={Inscription} />
          <Route path="/prediction" component={Prediction} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
