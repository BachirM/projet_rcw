const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const User = require('./models/user_model');
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const connection = mongoose.connection;

mongoose.connect('mongodb://localhost:27017/bd_prj_rcw', { useNewUrlParser: true });
connection.once('open', () => {
    console.log('Connected to MongoDB');
});

app.post('/api/add', async (req, res) => {
    try {
        const newUser = new User(req.body);
        await newUser.save();
        res.status(201).json({ success: true, message: 'Utilisateur ajouté avec succès' });
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de l\'ajout de l\'utilisateur', error });
    }
});

app.post('/api/find', async (req, res) => {
    try {
        const { email, mdp } = req.body;
        const user = await User.findOne({ email, mdp });
        if (user) {
            res.status(200).json({ success: true, message: 'Utilisateur trouvé', user });
        } else {
            res.status(404).json({ success: false, message: 'Utilisateur non trouvé' });
        }
    } catch (error) {
        res.status(500).json({ success: false, message: 'Erreur lors de la recherche de l\'utilisateur', error });
    }
});

const PORT = 3164;
app.listen(PORT, () => {
    console.log(`J'écoute le port ${PORT}!`);
});
