from flask import Flask, request, jsonify
from flask_cors import CORS
from joblib import load
import numpy as np

app = Flask(__name__)
CORS(app)

try:
    model = load('sentiment_model.joblib')
except Exception as e:
    print(f"Error loading model: {e}")
    model = None

diseases = {
    "Maladie d'Alzheimer": ["Perte de memoire", "Desorientation", "Troubles du langage", "Troubles du comportement", "Changements de personnalite"],
    "Maladie de Parkinson": ["Tremblements", "Raideur musculaire", "Ralentissement des mouvements", "Troubles de l'equilibre", "Problemes de coordination"],
    "Diabete": ["Soif excessive", "Mictions frequentes", "Fatigue", "Vision floue", "Engourdissement des pieds"],
    "Asthme": ["Respiration sifflante", "Toux", "Essoufflement", "Oppression thoracique", "Augmentation de la frequence cardiaque"],
    "Arthrite": ["Douleur articulaire", "Raideur", "Gonflement", "Perte de mobilite", "Rougeur et chaleur articulaires"],
    "Hypertension arterielle": ["Maux de tête", "Vision floue", "Fatigue", "Palpitations", "Essoufflement"],
    "Infections respiratoires": ["Fievre", "Toux", "Congestion nasale", "Essoufflement", "Douleur thoracique"],
    "Infections urinaires": ["Douleur en urinant", "Envie fréquente d'uriner", "Fievre", "Douleur abdominale", "Urine trouble"],
    "Rhume": ["Congestion nasale", "Eternuements", "Toux", "Mal de gorge", "Ecoulement nasal"],
    "Steatose hepatique": ["Fatigue", "Douleurs abdominales", "Perte d'appetit", "Prise de poids", "Gonflement de l'abdomen"],
    "Colopathie": ["Douleurs abdominales", "Diarrhee", "Constipation", "Gaz intestinaux", "Ballonnements"]
}

@app.route('/prediction', methods=['POST'])
def prediction():
    try:
        data = request.json
        print(data)
        
        if 'age' not in data or 'symptomes' not in data:
            raise ValueError("Invalid input format. 'age' and 'symptomes' are required.")

        age = float(data['age'])
        symptomes = np.array(list(data['symptomes'].values())).reshape(1, -1)

        if model is None:
            #print
            raise ValueError("Model not loaded.")
        print(1)
        prediction_index = model.predict(symptomes)[0]
        predicted_disease = list(diseases.keys())[prediction_index]
        predicted_symptoms = diseases[predicted_disease]

        print(predicted_disease)

        return jsonify({'prediction': predicted_disease, 'predicted_symptoms': predicted_symptoms})
    

    except ValueError as ve:
        return jsonify({'error': str(ve)})

    except Exception as e:
        return jsonify({'error': str(e)})

if __name__ == '__main__':
    app.run(debug=True, port=5000)
