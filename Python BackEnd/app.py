from flask import Flask, request, jsonify
from flask_cors import CORS
import joblib as jlb 
import numpy as np

# Use Myenv Environnememnt

app = Flask(__name__)
CORS(app)  # Active CORS pour toutes les routes


model = jlb.load('sentiment_model.joblib')

@app.route('/prediction', methods=['POST'])
def prediction():
    try:
        data = request.json  # Récupère les données JSON du front-end
        print(data)
        # Formate les données pour la prédiction
        age = float(data['age'])
        symptomes = np.array(list(data['symptomes'].values())).reshape(1, -1)

        # Fait la prédiction avec le modèle
        prediction = model.predict(symptomes)

        # Retourne la prédiction au format JSON
        return jsonify({'prediction': int(prediction[0])})

    except Exception as e:
        return jsonify({'error': str(e)})

if __name__ == '__main__':
    app.run(debug=True, port=5000)
